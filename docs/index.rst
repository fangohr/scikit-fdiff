Welcome to Scikit FiniteDiff's documentation!
=============================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction
   overview
   advanced/index.rst
   auto_examples/index.rst
   modules
   cite
   news_and_roadmap
   contribute


.. include:: introduction.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
