skfdiff.plugins package
=======================

Submodules
----------

skfdiff.plugins.container module
--------------------------------

.. automodule:: skfdiff.plugins.container
    :members:
    :undoc-members:
    :show-inheritance:

skfdiff.plugins.displays module
-------------------------------

.. automodule:: skfdiff.plugins.displays
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: skfdiff.plugins
    :members:
    :undoc-members:
    :show-inheritance:
