#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pytest

from skfdiff import Model


@pytest.mark.parametrize("backend", ("numpy", "numba"))
def test_model_monovariate(backend):
    model = Model(
        "k * dxxU", "U", "k(x)", boundary_conditions="periodic", backend=backend
    )
    x, dx = np.linspace(-np.pi, np.pi, 100, retstep=True, endpoint=False)
    U = np.cos(x)
    k = np.ones_like(U)
    fields = model.Fields(x=x, U=U, k=k)
    F = model.F(fields)
    model.J(fields)

    assert np.isclose(F, -np.cos(x), atol=1e-3).all()


@pytest.mark.parametrize("backend", ("numpy", "numba"))
def test_model_monovariate_2D(backend):
    model = Model(
        "k * (dxxU + dyyU)",
        "U(x, y)",
        "k(x, y)",
        boundary_conditions="periodic",
        backend=backend,
    )

    x, dx = np.linspace(-np.pi, np.pi, 256, retstep=True, endpoint=False)
    y, dy = np.linspace(-np.pi, np.pi, 128, retstep=True, endpoint=False)
    xx, yy = np.meshgrid(x, y, indexing="ij")
    U = np.cos(xx) + np.sin(yy)
    k = np.ones_like(U)
    fields = model.Fields(x=x, y=y, U=U, k=k)
    dtU = model.fields_from_U(model.F(fields), fields).U
    model.J(fields)

    assert np.isclose(dtU.values, (-np.cos(xx) - np.sin(yy)), atol=1e-3).all()


@pytest.mark.parametrize("backend", ("numpy", "numba"))
@pytest.mark.parametrize(
    "bc",
    (
        "periodic",
        ("dirichlet", "dxU - 2"),
        (None, "dirichlet"),
        ("dxU - (U + 2)", None),
    ),
)
def test_model_bc(backend, bc):
    model = Model("dxxxU", "U", boundary_conditions={("U", "x"): bc}, backend=backend)
    x = np.linspace(0, 10, 100, endpoint=False)
    U = np.cos(x * 2 * np.pi / 10)
    fields = model.Fields(x=x, U=U)
    model.F(fields)
    model.J(fields)


@pytest.mark.parametrize("backend", ("numpy", "numba"))
def test_model_bivariate(backend):
    model = Model(
        ["k1 * dxx(v)", "k2 * dxx(u)"],
        ["u", "v"],
        ["k1", "k2"],
        boundary_conditions="periodic",
        backend=backend,
    )
    x, dx = np.linspace(0, 10, 100, retstep=True, endpoint=False)
    u = np.cos(x * 2 * np.pi / 10)
    v = np.sin(x * 2 * np.pi / 10)
    fields = model.Fields(x=x, u=u, v=v, k1=1, k2=1)
    F = model.F(fields)

    dxu = np.gradient(np.pad(u, 2, mode="wrap")) / dx
    dxxu = np.gradient(dxu) / dx
    dxu = dxu[2:-2]
    dxxu = dxxu[2:-2]

    dxv = np.gradient(np.pad(v, 2, mode="wrap")) / dx
    dxxv = np.gradient(dxv) / dx
    dxv = dxv[2:-2]
    dxxv = dxxv[2:-2]
    assert np.isclose(F, np.vstack([dxxv, dxxu]).flatten("F"), atol=1e-3).all()


@pytest.mark.parametrize("backend", ("numpy", "numba"))
@pytest.mark.parametrize(
    "args",
    [
        ("dxU", lambda x: -np.sin(x)),
        ("dxxU", lambda x: -np.cos(x)),
        ("dxxxU", lambda x: np.sin(x)),
        ("dxxxxU", lambda x: np.cos(x)),
    ],
)
def test_finite_diff(backend, args):
    symb_diff, analytical_func = args
    model = Model(symb_diff, "U", boundary_conditions="periodic")
    x = np.linspace(0, 2 * np.pi, 1000, endpoint=False)
    skfdiff_diff = model.F(model.Fields(x=x, U=np.cos(x)), dict(periodic=True))
    assert np.isclose(skfdiff_diff, analytical_func(x), rtol=1e-4, atol=1e-4).all()


@pytest.mark.parametrize("backend", ("numpy", "numba"))
@pytest.mark.parametrize("uporder", [1, 2, 3])
def test_upwind(backend, uporder):
    model = Model(evolution_equations=["upwind(U, U, x, %i)" % uporder], unknowns="U")

    x, dx = np.linspace(0, 10, 100, retstep=True, endpoint=False)
    U = np.cos(x * 2 * np.pi / 10)
    fields = model.Fields(x=x, U=U)
    model.F(fields)
    model.J(fields)


@pytest.mark.parametrize("backend", ("numpy", "numba"))
def test_special_mixed_coordinate(backend):
    model = Model("dxyU * y", "U(x, y)", boundary_conditions={("U", "x"): (None, None)})
    x = np.linspace(0, 10, 32)
    y = np.linspace(-np.pi, np.pi, 16)

    U = np.zeros((x.size, y.size))
    fields = model.Fields(x=x, y=y, U=U)
    model.F(fields)
    model.J(fields)


if __name__ == "__main__":
    test_model_bc("numpy", "periodic")
