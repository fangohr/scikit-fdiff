#!/usr/bin/env python
# coding=utf-8


def dirichlet_1D_bc_test():
    pass


def neumann_1D_bc_test():
    pass


def robin_1D_bc_test():
    pass


def periodic_1D_bc_test():
    pass


def dirichlet_2D_bc_test():
    pass


def neumann_2D_bc_test():
    pass


def robin_2D_bc_test():
    pass


def periodic_2D_bc_test():
    pass


def crossed_deriv_2D_bc_test():
    pass


def robin_with_opposite_deriv_2D_bc_test():
    pass
